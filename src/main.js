import Phaser from 'phaser'

import LocalExploration from '@scenes/LocalExploration'

const config = {
  type: Phaser.WEBGL,
  width: '100%',
  height: '100%',
  backgroundColor: '#FFFFAC',
  parent: 'mario',
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 0 },
      fixedStep: true,
      fps: 60,
      tileBias: 16,
      debug: true,
    },
  },
  scene: [
    LocalExploration,
  ],
  scale: {
    zoom: 4,
  },
  pixelArt: true,
}

export default new Phaser.Game(config)
