const GRAV = 1000
const ROOT2 = Math.pow(Math.sqrt(2), -1)
const MAX_V = 150

function worldCoordsToTileCoords (x, y) {
  return {
    x: x / 16,
    y: y / 16,
  }
}

function worldCoordsToTileCoordsInt (x, y) {
  return {
    x: parseInt(x / 16),
    y: parseInt(y / 16),
  }
}

function playAnimation (obj, animation, cb = () => {}) {
  obj.sprite.play(animation, true)
    .setFlipX(obj.facingDir.includes('west'))
  cb()
}

function createAnimation (scene, name, rowIdx, numFrames, frameOffset = 0, opts = {}) {
  scene.anims.create({
    key: name,
    frames: scene.anims.generateFrameNumbers('lea', {
      start: frameOffset + rowIdx * 36,
      end: frameOffset + rowIdx * 36 + numFrames - 1,
    }),
    frameRate: opts.frameRate || 12,
    repeat: opts.repeat >= 0 ? opts.repeat : -1
  })
}

export default class Hero {
  constructor (scene, x, y) {
    this.scene = scene
    this.sprite = this.scene.physics.add
      .sprite(x, y, 'lea')
      .setSize(16, 16)
      .setOffset(8, 16)
      .setDisplayOrigin(8, 16)

    // this.scene.heightMaps.forEach((l, idx) => {
    //   this.scene.physics.add.collider(this.sprite, l, null, (s, t) => {
    //     if (t.index < 0) {
    //       return false
    //     } else {
    //       return idx === 1
    //       // return idx > (this.posZ / 32.0)
    //     }
    //     // console.log(`C ${idx}:`, t, idx, this.posZ / 32.0, idx > (this.posZ / 32.0))
    //   })
    // })

    this.sprite.setCollideWorldBounds(true)

    this.scene.cameras.main.startFollow(this.sprite)

    createAnimation(this.scene, 'lea-run-east', 2, 6, 3)
    createAnimation(this.scene, 'lea-run-north', 0, 6, 3)
    createAnimation(this.scene, 'lea-run-south', 4, 6, 3)
    createAnimation(this.scene, 'lea-run-north-east', 1, 6, 3)
    createAnimation(this.scene, 'lea-run-south-east', 3, 6, 3)

    createAnimation(this.scene, 'lea-idle-east', 2, 1, 1)
    createAnimation(this.scene, 'lea-idle-north', 0, 1, 1)
    createAnimation(this.scene, 'lea-idle-south', 4, 1, 1)
    createAnimation(this.scene, 'lea-idle-north-east', 1, 1, 1)
    createAnimation(this.scene, 'lea-idle-south-east', 3, 1, 1)

    createAnimation(this.scene, 'lea-jump-east-start', 7, 1, 7)
    createAnimation(this.scene, 'lea-jump-north-start', 5, 1, 7)
    createAnimation(this.scene, 'lea-jump-south-start', 9, 1, 7)
    createAnimation(this.scene, 'lea-jump-north-east-start', 6, 1, 7)
    createAnimation(this.scene, 'lea-jump-south-east-start', 8, 1, 7)

    createAnimation(this.scene, 'lea-jump-east-end', 7, 1, 8)
    createAnimation(this.scene, 'lea-jump-north-end', 5, 1, 8)
    createAnimation(this.scene, 'lea-jump-south-end', 9, 1, 8)
    createAnimation(this.scene, 'lea-jump-north-east-end', 6, 1, 8)
    createAnimation(this.scene, 'lea-jump-south-east-end', 8, 1, 8)

    this.facingDir = 'east'
    this.animDir = 'east'
    this.currentLevel = 0
    this.isJumping = false

    // Initialize dynamics
    this.acc = { x: 0, y: 0, z: 0 }
    this.vel = { x: 0, y: 0, z: 0 }
    this.pos = { x: x, y: y, z: 0 }

    this.currTileCoords = null

    // Initial idle animation
    playAnimation(this, 'lea-idle-east')
  }

  update (t, dt) {
    this.updateMovement(t, dt)
  }

  updateMovement(t, dt) {
    this.currTileCoords = worldCoordsToTileCoords(this.pos.x, this.pos.y)

    // Draw debug rect for player
    this.debugTilesTouching()

    // ms per frame
    const dtPerSec = dt / 1000

    const cursors = this.scene.cursors
    const baseSpeed = 150

    let { x: px, y: py, z: pz } = this.pos
    let { x: ax, y: ay, z: az } = this.acc
    let { x: vx, y: vy, z: vz } = this.vel

    const currentTileLevel = this.scene.heightmap.getHeightAt(this.currTileCoords.x, this.currTileCoords.y)
    const currentTileHeight = currentTileLevel * 32 

    // Update accelation
    ax = 0
    ay = 0
    az = -1 * GRAV

    // Update velocity
    vx = 0
    vx += cursors.left.isDown ? -baseSpeed : 0
    vx += cursors.right.isDown ? baseSpeed : 0
    vx += ax * dtPerSec
    if (Math.abs(vx) > MAX_V) {
      vx = vx < 0 ? -1 * MAX_V : MAX_V
    }

    vy = 0
    vy += cursors.up.isDown ? -baseSpeed : 0
    vy += cursors.down.isDown ? baseSpeed : 0
    vy += ay * dtPerSec
    if (Math.abs(vy) > MAX_V) {
      vy = vy < 0 ? -1 * MAX_V : MAX_V
    }

    vz += az * dtPerSec

    let isMoving = vx !== 0 || vy !== 0
    let isMovingDiagonally = vx !== 0 && vy !== 0
    let movementFactor = isMovingDiagonally ? Math.pow(Math.sqrt(2), -1) : 1

    // Update position
    px += vx * movementFactor * dtPerSec
    py += vy * movementFactor * dtPerSec
    pz += vz * dtPerSec

    // Override the Z dimension if we are on solid ground
    if (pz <= currentTileHeight) {
      pz = currentTileHeight
      vz = 0
      az = 0
      this.isJumping = false
    }

    // Determine facing direction based on velocity
    if (vx === 0 && vy < 0) {
      this.facingDir = 'north'
    } else if (vx === 0 && vy > 0) {
      this.facingDir = 'south'
    } else if (vx < 0 && vy === 0) {
      this.facingDir = 'west'
    } else if (vx > 0 && vy === 0) {
      this.facingDir = 'east'
    } else if (vx > 0 && vy > 0) {
      this.facingDir = 'south-east'
    } else if (vx > 0 && vy < 0) {
      this.facingDir = 'north-east'
    } else if (vx < 0 && vy > 0) {
      this.facingDir = 'south-west'
    } else if (vx < 0 && vy < 0) {
      this.facingDir = 'north-west'
    }

    // Determine animation direction
    // Should be same for all animations except for left-facing
    // The left-facing animations using the flipped version
    // of the right-facing animation.
    this.animDir = this.facingDir.replace('west', 'east')

    if (this.isJumping) {
      playAnimation(this, `lea-jump-${this.animDir}`)
      // this.sprite.play(`lea-jump-${animDir}`, false).setFlipX(flipAnimation)
    } else if (isMoving) {
      playAnimation(this, `lea-run-${this.animDir}`)
    } else {
      playAnimation(this, `lea-idle-${this.animDir}`)
    }

    // Override animation if we are falling vertically
    if (this.vel.z < 0) {
      playAnimation(this, `lea-jump-${this.animDir}-end`)
    }

    // Maybe use this later??
    // console.log('t', this.currTileCoords.y + velYPerFrame / 16)
    // const nextTile = this.scene.layers.heightmap.getTileAt(
    //   parseInt(this.currTileCoords.x + velXPerFrame / 16),
    //   parseInt(this.currTileCoords.y + velYPerFrame / 16)
    // )
    // console.log('Next Tile', nextTile, this.currTileCoords.y, velYPerFrame / 16)
    // if (nextTile && (nextTile.index - 257) * 32 > this.posZ) {
    //   vx = 0
    //   vy = 0
    // }
    // this.sprite.setVelocity(vx * movementFactor, vy * movementFactor)

    // Save dynamics values
    this.acc = { x: ax, y: ay, z: az }
    this.vel = { x: vx, y: vy, z: vz }
    this.pos = { x: px, y: py, z: pz }

    // Update sprite position
    this.sprite.setDepth(parseInt(this.pos.z / 32) * 2 + 1)
    this.sprite.setPosition(this.pos.x, this.pos.y - this.pos.z * ROOT2)

    // Maybe use layer??
    // const heightAtCurrentTile = this.scene.heightmap.getHeightAt(this.currTileCoords.x, this.currTileCoords.y) * 32
    // waw
    // const velXPerFrame = vx * movementFactor * dtPerSec
    // const velYPerFrame = vy * movementFactor * dtPerSec

    // if (isMoving) {
    //   this.scene.debug('PZ', this.posZ)
    //   const nextTile = this.scene.layers.heightmap.getTileAt(
    //     parseInt(this.currTileCoords.x + velXPerFrame / 16),
    //     parseInt(this.currTileCoords.y + velYPerFrame / 16)
    //   )

    //   // console.log('nextTile', nextTile)
    //   const heightAtNextTile = this.scene.heightmap.getHeightAt(nextTile.x + 1, nextTile.y) * 32
    //   // this.scene.debug('E', this.currTileCoords.x, heightAtCurrentTile, parseInt(this.currTileCoords.x + velXPerFrame / 16), heightAtNextTile)
    // }

    // this.velZ = vz

    // this.posZ += this.velZ * dtPerSec

    // // if ()
    // if (this.posZ <= heightAtCurrentTile) {
    //   this.posZ = heightAtCurrentTile
    //   this.velZ = 0
    //   this.isJumping = false
    // }
    // if (this.currentLevel === 1 && this.velZ < 0 && this.posZ <= 32) {
    // // if (this.posZ <= (this.currentLevel - 1) * 32) {
    //   this.posZ = 32
    //   this.velZ = 0
    //   this.isJumping = false
    // }


    // if (this.isJumping) {
    //   this.sprite.body.position.y += -1 * this.vel.z * dtPerSec * ROOT2
    // }

    this.currTileCoords = worldCoordsToTileCoords(this.pos.x, this.pos.y)
  }

  // Initiate jump
  jump () {
    this.currentLevel = 1
    this.isJumping = true
    this.vel.z = 250

    this.animDir = this.facingDir.replace('west', 'east')
    playAnimation(this, `lea-jump-${this.animDir}-start`)
  }

  // NOTE: Only works for rectangular boxes
  // Draw box over all tiles the hero is touching currently
  debugTilesTouching () {
    if (!this.debugRects) {
      this.debugRects = []
    }

    this.debugRects.forEach(r => r.destroy())
    this.debugRects = []

    let { x, y, body: { width, height } } = this.sprite

    if (x < 0 || y < 0) {
      return
    }

    const minTile = worldCoordsToTileCoordsInt(x, y)
    const maxTile = worldCoordsToTileCoordsInt(x + width, y + height)

    this.debugRects.push(
      this.scene.add.rectangle(minTile.x * 16, minTile.y * 16, (maxTile.x - minTile.x + 1) * 16, (maxTile.y - minTile.y + 1) * 16, '#000', 0.2).setOrigin(0)
    )

    this.debugRects.push(
      this.scene.add.rectangle(parseInt(this.currTileCoords.x) * 16, parseInt(this.currTileCoords.y) * 16, 16, 16, 0xFF0000, 0.2).setOrigin(0)
    )
  }
}