import Phaser from 'phaser'
import Hero from '../Hero'
import Heightmap from '../Heightmap'

import { debounce } from 'lodash-es'

const HEIGHTMAP = []

export default class LocalExploration extends Phaser.Scene {
  constructor (config) {
    super(config)
    this.debug = debounce(this._d, this.timerMax)
  }

  preload () {
    this.load.setBaseURL('http://localhost:1338')
    this.load.image('autumn', 'maps/autumn.png')
    this.load.tilemapTiledJSON('map', 'maps/world-map-1.json')
    this.load.spritesheet('lea', 'lea.png', {
      frameWidth: 32,
      frameHeight: 32,
      margin: 2,
    })
  }

  create () {
    this.map = this.make.tilemap({ key: 'map' })
    this.tileset = this.map.addTilesetImage('autumn', 'autumn', 16, 16)

    this.layers = {
      heightmap: this.map.createLayer('heightmap', this.tileset, 0, 0),
      ground: this.map.createLayer('ground', this.tileset, 0, 0),
      ground1: this.map.createLayer('ground+1', this.tileset, 0, 0),
    }

    // this.layers.ground.setDepth(0)
    // this.layers.ground1.setDepth(2)

    this.layers.heightmap.setAlpha(0)
    this.heightmap = new Heightmap(this.layers.heightmap)

    // this.layers.heightmap.forEachTile(tile => {
    //   const idx = tile.index - 257
    //   console.log('TILE', tile)
    //   if (idx >= 1) {
    //     this.heightMaps[idx - 1].putTileAt(tile, tile.x, tile.y)
    //   }
    // })

    this.layers.heightmap.setAlpha(0)
    this.layers.heightmap.setCollisionByExclusion([-1], false)

    this.cursors = this.input.keyboard.addKeys({
      up: Phaser.Input.Keyboard.KeyCodes.W,
      down: Phaser.Input.Keyboard.KeyCodes.S,
      left: Phaser.Input.Keyboard.KeyCodes.A,
      right: Phaser.Input.Keyboard.KeyCodes.D,
    })

    this.input.keyboard.addKey('Space')
      .on('down', evt => this.hero.jump())

    this.hero = new Hero(this, 32, 32)

    this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels)
  }

  update (t, dt) {
    this.hero.update(t, dt)
  }

  _d (/* ... */) {
    console.log('DEBUG', ...arguments)
  }
}
